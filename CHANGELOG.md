# 0.11.0 (2022-04-29)

- **[Feature]** Add methods `tmul` and `tmulAssign` to `ColorMatrix`.
- **[Change]** Mutating `ColorMatrix` methods now return the result for easier chaining.
- **[Change]** Make `ColorMatrix` an abstract.

# 0.10.0 (2021-04-21)

- **[Breaking change]** Update to `patchman@0.10.3`.
- **[Internal]** Update to Yarn 2.

# 0.9.0 (2020-09-02)

- **[Breaking change]** Update to `patchman@0.9.0`.
- **[Breaking change]** Rename from `@patchman/color-matrix` to `@patchman/color_matrix`.

# 0.8.0 (2020-01-08)

- **[Breaking change]** Update to `patchman@0.8.0`.

# 0.7.0 (2020-01-07)

- **[Feature]** First release.
