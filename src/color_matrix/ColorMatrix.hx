package color_matrix;

import etwin.Error;
import etwin.flash.filters.ColorMatrixFilter;

abstract ColorMatrix(Array<Float>) {
  private static var IDENTITY: ColorMatrix = new ColorMatrix([
    1, 0, 0, 0, 0,
    0, 1, 0, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 1, 0
  ]);

  private static var ZERO: ColorMatrix = new ColorMatrix([
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0
  ]);

  private inline function new(matrix: Array<Float>) {
    this = matrix;
  }

  private inline function asRaw(): Array<Float> {
    return this;
  }

  /**
   * Clones this matrix.
   */
  public inline function clone(): ColorMatrix {
    return new ColorMatrix(this.copy());
  }

  /**
   * Returns a pretty multi-line representation of this matrix.
   */
  public function toString(): String {
    var parts: Array<String> = [];
    var sep = "[[";
    for (i in 0...4) {
      parts.push(sep);
      parts.push(this.slice(5 * i, 5 * (i + 1)).join(" "));
      sep = "]\n[";
    }
    parts.push("]]");
    return parts.join("");
  }

  /**
   * Returns the corresponding `ColorMatrixFilter`.
   */
  public inline function toFilter(): ColorMatrixFilter {
    return new ColorMatrixFilter(this);
  }

  /**
   * Returns a new identity matrix.
   */
  public static inline function identity(): ColorMatrix {
    return IDENTITY.clone();
  }

  /**
   * Returns a new zero matrix.
   */
  public static inline function zero(): ColorMatrix {
    return ZERO.clone();
  }

  /**
   * Returns a new matrix backed by the provided array.
   */
  public static function of(matrix: Array<Float>): ColorMatrix {
    if (matrix.length != 20) {
      throw new Error("AssertionError: `ColorMatrix.of` expected `matrix.length == 20");
    }
    return new ColorMatrix(matrix);
  }

  /**
   * Computes the matrix product `this * other` in place, and returns the result.
   */
  public inline function mulAssign(other: ColorMatrix): ColorMatrix {
    return new ColorMatrix(mulFull(asRaw(), this, other.asRaw()));
  }

  /**
   * Computes the matrix product `other * this` in place, and returns the result.
   */
  public inline function tmulAssign(other: ColorMatrix): ColorMatrix {
    return new ColorMatrix(mulFull(this, other.asRaw(), this));
  }

  /**
   * Computes the matrix product `this * other`.
   */
  public inline function mul(other: ColorMatrix): ColorMatrix {
    return new ColorMatrix(mulFull(this.copy(), this, other.asRaw()));
  }

  /**
   * Computes the matrix product `other * this`.
   */
   public inline function tmul(other: ColorMatrix): ColorMatrix {
    return new ColorMatrix(mulFull(this.copy(), other.asRaw(), this));
  }

  /**
   * Applies the corresponding HSV transformation to the matrix, and returns the result.
   */
  public inline function applyHsv(h: Float, s: Float, v: Float): ColorMatrix {
    return mulAssign(ColorMatrix.fromHsv(h, s, v));
  }

  /**
   * Computes `a * b` into `dest` and returns the result.
   */
  private static function mulFull(dest: Array<Float>, a: Array<Float>, b: Array<Float>): Array<Float> {
    // Local variables to store matrix elements.
    // This makes sure that the multiplication works even if the input arrays alias.
    var a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19;
    var b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19;

    dest[0] = (a0=a[0])*(b0=b[0]) + (a1=a[1])*(b5=b[5]) + (a2=a[2])*(b10=b[10]) + (a3=a[3])*(b15=b[15]) + (a4=a[4]);
    dest[1] = a0*(b1=b[1]) + a1*(b6=b[6]) + a2*(b11=b[11]) + a3*(b16=b[16]) + a4;
    dest[2] = a0*(b2=b[2]) + a1*(b7=b[7]) + a2*(b12=b[12]) + a3*(b17=b[17]) + a4;
    dest[3] = a0*(b3=b[3]) + a1*(b8=b[8]) + a2*(b13=b[13]) + a3*(b18=b[18]) + a4;
    dest[4] = a0*(b4=b[4]) + a1*(b9=b[9]) + a2*(b14=b[14]) + a3*(b19=b[19]) + a4;

    dest[5] = (a5=a[5])*b0 + (a6=a[6])*b5 + (a7=a[7])*b10 + (a8=a[8])*b15 + (a9=a[9]);
    dest[6] = a5*b1 + a6*b6 + a7*b11 + a8*b16 + a9;
    dest[7] = a5*b2 + a6*b7 + a7*b12 + a8*b17 + a9;
    dest[8] = a5*b3 + a6*b8 + a7*b13 + a8*b18 + a9;
    dest[9] = a5*b4 + a6*b9 + a7*b14 + a8*b19 + a9;

    dest[10] = (a10=a[10])*b0 + (a11=a[11])*b5 + (a12=a[12])*b10 + (a13=a[13])*b15 + (a14=a[14]);
    dest[11] = a10*b1 + a11*b6 + a12*b11 + a13*b16 + a14;
    dest[12] = a10*b2 + a11*b7 + a12*b12 + a13*b17 + a14;
    dest[13] = a10*b3 + a11*b8 + a12*b13 + a13*b18 + a14;
    dest[14] = a10*b4 + a11*b9 + a12*b14 + a13*b19 + a14;

    dest[15] = (a15=a[15])*b0 + (a16=a[16])*b5 + (a17=a[17])*b10 + (a18=a[18])*b15 + (a19=a[19]);
    dest[16] = a15*b1 + a16*b6 + a17*b11 + a18*b16 + a19;
    dest[17] = a15*b2 + a16*b7 + a17*b12 + a18*b17 + a19;
    dest[18] = a15*b3 + a16*b8 + a17*b13 + a18*b18 + a19;
    dest[19] = a15*b4 + a16*b9 + a17*b14 + a18*b19 + a19;

    return dest;
  }

  /**
   * Returns the matrix representing the corresponding HSV transformation.
   *
   * @param h: Hue
   * @param s: Saturation
   * @param v: Value
   */
  public static function fromHsv(h: Float, s: Float, v: Float): ColorMatrix {
    h *= Math.PI / 180;
    var vsu: Float = v * s * Math.cos(h);
    var vsw: Float = v * s * Math.sin(h);
    var matrix: Array<Float> = [
      0.299 * v + 0.701 * vsu + 0.168 * vsw, 0.587 * v - 0.587 * vsu + 0.330 * vsw, 0.114 * v - 0.114 * vsu - 0.497 * vsw, 0, 0,
      0.299 * v - 0.299 * vsu - 0.328 * vsw, 0.587 * v + 0.413 * vsu + 0.035 * vsw, 0.114 * v - 0.114 * vsu + 0.292 * vsw, 0, 0,
      0.299 * v - 0.300 * vsu + 1.250 * vsw, 0.587 * v - 0.588 * vsu - 1.050 * vsw, 0.114 * v + 0.886 * vsu - 0.203 * vsw, 0, 0,
      0, 0, 0, 1, 0
    ];
    return new ColorMatrix(matrix);
  }
}
